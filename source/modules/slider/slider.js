import { $, $$ } from "@/modules/_utils/dom/select";
import Swiper from "swiper";

export default () => {
  const swipers = $$("[data-swiper-id]");

  Array.prototype.forEach.call(swipers, swiper => {
    const id = swiper.getAttribute("data-swiper-id");
    const row = swiper.getAttribute("data-swiper-row");

    if (swiper.getAttribute("data-swiper-row") === "4") {
      new Swiper(swiper, {
        slidesPerView: 4,
        slidesPerGroup: 3,
        pagination: {
          el: ".swiper-pagination",
          type: "bullets",
          clickable: true
        },
        navigation: {
          nextEl: ".main-slider__arrow--right",
          prevEl: ".main-slider__arrow--left"
        }
      });
    } else if (swiper.getAttribute("data-swiper-row") === "3") {
      if (swiper.getAttribute("data-swiper-id") === "newsSlider") {
        new Swiper(swiper, {
          slidesPerView: 3,
          slidesPerGroup: 3,

          pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
          },
          navigation: {
            nextEl: ".new-slider__arrow--right.news",
            prevEl: ".new-slider__arrow--left.news"
          }
        });
      } else {
        new Swiper(swiper, {
          slidesPerView: 3,
          slidesPerGroup: 3,

          pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
          },
          navigation: {
            nextEl: ".new-slider__arrow--right.sales",
            prevEl: ".new-slider__arrow--left.sales"
          }
        });
      }
    } else {
      new Swiper(swiper, {
        pagination: {
          el: ".swiper-pagination",
          type: "bullets",
          clickable: true
        },
        navigation: {
          nextEl: "#sliderRight",
          prevEl: "#sliderLeft"
        }
      });
    }
  });
};
