import init from "../_utils/plugin-init";

class TestMod {
  constructor(element, options) {
    this.element = element;
    this.name = "test-mod";

    this._defaults = {};

    this.options = {
      ...this._defaults,
      ...options
    };

    this.init();
  }

  init() {
    this.buildCache();
    this.bindEvents();
  }

  buildCache() {}

  bindEvents() {}
}

export default init(TestMod);
